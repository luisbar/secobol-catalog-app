# Tareas por hacer

- [x] Reducir el tamaño del fondo del collapsingtoolbar
- [x] Areglar el layout para la orientacion landscape
- [x] Cambiar el color de fondo al modal
- [ ] Revisar y refactorizar el codigo
- [x] Modal se cierra al voltear la pantalla
- [x] Actualizar estado solo en el padre o en padre e hijos, si se actualiza solo en el padre, no se encapsula toda la logica en sus hijos, si actualizamos en los hijos, puede ser que afecte al rendimiento
- [x] Verificar si la imagen existe antes de cargarla
- [ ] Calcular el total del total 

# Cambios en node_modules

- Error con la libreraia de Toast acerca EventEmitter, cambiar en node_modules/react-native-root-siblings/lib/AppRegistryInjection.js

```
//import EventEmitter from 'react-native/Libraries/vendor/emitter/EventEmitter'
import EventEmitter from 'react-native/Libraries/EventEmitter/EventEmitter';
```

- Añadir a react-native-root-toast

```
static propTypes = {
    ...ViewPropTypes,
    containerStyle: ViewPropTypes.style,
    duration: PropTypes.number,
    visible: PropTypes.bool,
    position: PropTypes.number,
    animation: PropTypes.bool,
    shadow: PropTypes.bool,
    backgroundColor: PropTypes.string,
    opacity: PropTypes.number,
    shadowColor: PropTypes.string,
    textColor: PropTypes.string,
    textStyle: Text.propTypes.style,
    delay: PropTypes.number,
    hideOnPress: PropTypes.bool,
    onHide: PropTypes.func,
    onHidden: PropTypes.func,
    onShow: PropTypes.func,
    onShown: PropTypes.func,
    customStyle: ViewPropTypes.style,
};

...

style={[
    styles.defaultStyle,
    position,
    this.props.customStyle
]}
```

- Despues de installar todos las dependencias modificar el archivo style de la libreria react-native-collapsingtoolbar

```
left: {
  paddingTop: 21,
  top: 0,
  left: 0,
  width: 50,
  height: 56,
  alignItems: 'center',
  justifyContent: 'center',
},
right: {
  paddingTop: 21,
  top: 0,
  right: 0,
  height: 56,
  minWidth: 56,
  alignItems: 'center',
  justifyContent: 'center',
},
title: {
  paddingTop: 30,
  paddingLeft: 25,
  fontFamily: 'Nobel-Bold',
  fontSize: 14,
},
```

- Despues de installar todos las dependencias modificar el archivo index.js de la libreria react-native-collapsingtoolbar, antes del title

```
const titleScale = this.state.scrollY.interpolate({
  inputRange: [0, scrollDistance / 2, scrollDistance],
  outputRange: [1, 1, 1],
  extrapolate: 'clamp',
});

...

{
  this.props.number &&
  <View style={{ borderRadius: 10, backgroundColor: '#C83B0E', width: 20, height: 20, overflow: 'hidden', alignItems: 'center', justifyContent: 'center', position: 'absolute', top: 34, left: 42 }}><Text style={{color: '#FFF'}}>{this.props.number}</Text></View>
}
<Text style={[styles.title,{color: titleColor}]}>{title}</Text>
```