/***********************
 * Node modules import *
 ***********************/
import { combineReducers } from 'redux';
/******************
 * Project import *
 ******************/
import cutReducer from './cut/reducer.js';
import shoppingCartReducer from './shoppingCart/reducer.js';

export default combineReducers({
  cutReducer,
  shoppingCartReducer,
});
