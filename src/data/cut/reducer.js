/******************
 * Project import *
 ******************/
import {
  WILL_GET_CUTS,
  GET_CUTS,
  DID_GET_CUTS,
  
  SHOW_PROGRESS,
} from './actionType.js';
/*************
 * Constants *
 *************/
const initialState = {
  cuts: [],
  statusOfGettingCuts: {
    isGettingCuts: false,
    wasError: false,
  },
  taskProgress: {
    total: 0,
    totalFinished: 0,
    progress: 0,
  }
};
/**
 * It manages all about cuts
 */
export default function cuts(state = initialState, action) {

  switch (action.type) {

    case WILL_GET_CUTS:
      return Object.assign({}, state, {
        statusOfGettingCuts: {
          isGettingCuts: true,
          wasError: false,
        },
      });

    case GET_CUTS:
      if (action.error)
        return Object.assign({}, state, {
          statusOfGettingCuts: {
            isGettingCuts: true,
            wasError: true,
          },
        });
      else
        return Object.assign({}, state, {
          cuts: action.payload,
        });

    case DID_GET_CUTS:
      return Object.assign({}, state, {
        statusOfGettingCuts: {
          isGettingCuts: false,
          wasError: false,
        },
      });
      
    case SHOW_PROGRESS:
      return Object.assign({}, state, {
        taskProgress: {
          total: action.payload.total ? action.payload.total : state.taskProgress.total,
          totalFinished: action.payload.totalFinished ? action.payload.totalFinished : state.taskProgress.totalFinished,
          progress: action.payload.progress ? action.payload.progress : state.taskProgress.progress,
        }
      });

    default:
      return state;
  }
}
