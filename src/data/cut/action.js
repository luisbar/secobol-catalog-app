/******************
 * Project import *
 ******************/
import {
  WILL_GET_CUTS,
  GET_CUTS,
  DID_GET_CUTS,

  SHOW_PROGRESS
} from './actionType.js';

import {
  realm,
  realTimeDatabase,
  fileManager
} from '../../service/index.js';

import {
  cleanShoppingCart
} from '../shoppingCart/action.js';
/**
 * It invokes a class which connects with a database in the cloud
 * in order to get all cuts of beef
 */
export function executeGetCuts(overrideData) {

  return dispatch => {
    dispatch(willGetCuts());

    if (realm.objects('Cut').length == 0 || overrideData) {//there is no data in database or update data

      realTimeDatabase.getCuts(
        function (cuts) {

          realm.write(() => {
            try {

              if (overrideData)//if it is an update of data
                realm.deleteAll();

              modifyCutsObjectAndSaveIt(cuts)

              dispatch(getCuts(false, realm.objects('Cut').sorted('number')));
              dispatch(didGetCuts());
              dispatch(cleanShoppingCart())

            } catch (error) {
              realm.deleteAll();
              dispatch(getCuts(true, error));
              dispatch(didGetCuts());
            }
          });
        },

        function (error) {
          dispatch(getCuts(true, error));
          dispatch(didGetCuts());
        }
      );
    } else {
      dispatch(getCuts(false, realm.objects('Cut').sorted('number')));
      dispatch(didGetCuts());
    }
  };
}
/**
 * It modifies the cuts object for saving it in database
 */
function modifyCutsObjectAndSaveIt(cuts) {

  Object.keys(cuts).map((key) => {
    let newUsages = [];
    let newSubCuts = [];
    const usages = cuts[key].usages;

    //To check the usages and modify them
    if (usages.griddle)
      newUsages[newUsages.length] = { value: usages.griddle.value };
    if (usages.grill)
      newUsages[newUsages.length] = { value: usages.grill.value };
    if (usages.pot)
      newUsages[newUsages.length] = { value: usages.pot.value };
    if (usages.oven)
      newUsages[newUsages.length] = { value: usages.oven.value };
    if (usages.skillet)
      newUsages[newUsages.length] = { value: usages.skillet.value };
    if (usages.tritured)
      newUsages[newUsages.length] = { value: usages.tritured.value };
    //To modify the subcuts to an array
    if (cuts[key].subCuts)
      Object.keys(cuts[key].subCuts).map((subCutKey) => {
        newSubCuts[newSubCuts.length] = {
          id: subCutKey,
          name: cuts[key].subCuts[subCutKey].name,
          description: cuts[key].subCuts[subCutKey].description,
          price: parseFloat(cuts[key].subCuts[subCutKey].price),
          mainImage: cuts[key].subCuts[subCutKey].mainImage,
          mainImageName: cuts[key].subCuts[subCutKey].mainImageName,
        }
      })

    cuts[key].id = key;
    cuts[key].number = parseInt(cuts[key].number);
    cuts[key].price = parseFloat(cuts[key].price);
    cuts[key].usages = newUsages;
    cuts[key].subCuts = newSubCuts;

    realm.create('Cut', cuts[key], true);
  });
}
/**
 * It changes a flag for knowing that cuts of beef have been fetching
 */
function willGetCuts() {

  return {
    type: WILL_GET_CUTS,
  };
}
/**
 * It changes the cuts object with the data fetched from cloud or
 * sends an error object when was an error
 */
function getCuts(error, payload) {

  return {
    type: GET_CUTS,
    payload: payload,
    error: error,
  };
}
/**
 * It restores the deault data
 */
function didGetCuts() {

  return {
    type: DID_GET_CUTS,
  };
}
/**
 * It updates the progress object for knowing the
 * percentage about a task in background
 */
export function showProgress(total, totalFinished, progress) {

  return {
    type: SHOW_PROGRESS,
    payload: {
      total: total,
      totalFinished: totalFinished,
      progress: progress,
    }
  }
}
