export const WILL_GET_CUTS = 'WILL_GET_CUTS';
export const GET_CUTS = 'GET_CUTS';
export const DID_GET_CUTS = 'DID_GET_CUTS';

export const SHOW_PROGRESS = 'SHOW_PROGRESS';
