/******************
 * Project import *
 ******************/
import {
  WILL_ADD_ITEM_TO_SHOPPING_CART,
  ADD_ITEM_TO_SHOPPING_CART,
  DID_ADD_ITEM_TO_SHOPPING_CART,

  WILL_REMOVE_ITEM_FROM_SHOPPING_CART,
  REMOVE_ITEM_FROM_SHOPPING_CART,
  DID_REMOVE_ITEM_FROM_SHOPPING_CART,
  
  WILL_UPDATE_ITEM_FROM_SHOPPING_CART,
  UPDATE_ITEM_FROM_SHOPPING_CART,
  DID_UPDATE_ITEM_FROM_SHOPPING_CART,
  
  CLEAN_SHOPPING_CART
} from './actionType.js';
/*************
 * Constants *
 *************/
const initialState = {
  shoppingCart: [],
  statusOfAddingAnItem: {
    isAddingAnItem: false,
    wasError: false,
  },
  statusOfRemovingAnItem: {
    isRemovingAnItem: false,
    wasError: false,
  },
  statusOfUpdatingAnItem: {
    isUpdatingAnItem: false,
    wasError: false,
  },
};
/**
 * It manages all about shopping cart
 */
export default function cuts(state = initialState, action) {

  switch (action.type) {
    
    case WILL_ADD_ITEM_TO_SHOPPING_CART:
      return Object.assign({}, state, {
        statusOfGettingCuts: {
          isGettingCuts: true,
          wasError: false,
        },
      });

    case ADD_ITEM_TO_SHOPPING_CART:
      if (action.error)
        return Object.assign({}, state, {
          statusOfRemovingAnItem: {
            isRemovingAnItem: true,
            wasError: true,
          },
        });
      else
        if (!thereIsItem(action.payload, state))//If the item is not in shopping cart
          return Object.assign({}, state, {
            statusOfAddingAnItem: {
              isAddingAnItem: true,
              wasError: false,
            },
            shoppingCart: Object.assign([], state.shoppingCart, {
              [state.shoppingCart.length]: action.payload,
            }),
          });
        else
          return state;
        
    case DID_ADD_ITEM_TO_SHOPPING_CART:
      return Object.assign({}, state, {
        statusOfGettingCuts: {
          isGettingCuts: false,
          wasError: false,
        },
      });
      
    case WILL_REMOVE_ITEM_FROM_SHOPPING_CART:
      return Object.assign({}, state, {
        statusOfRemovingAnItem: {
          isRemovingAnItem: true,
          wasError: false,
        },
      });

    case REMOVE_ITEM_FROM_SHOPPING_CART:
      if (action.error)
        return Object.assign({}, state, {
          statusOfRemovingAnItem: {
            isRemovingAnItem: true,
            wasError: true,
          },
        });
      else
        return Object.assign({}, state, {
          statusOfAddingAnItem: {
            isAddingAnItem: true,
            wasError: false,
          },
          shoppingCart: state.shoppingCart.filter((item, index) => item.id !== action.payload.id),
        });
        
    case DID_REMOVE_ITEM_FROM_SHOPPING_CART:
      return Object.assign({}, state, {
        statusOfRemovingAnItem: {
          isRemovingAnItem: false,
          wasError: false,
        },
      });
      
    case WILL_UPDATE_ITEM_FROM_SHOPPING_CART:
      return Object.assign({}, state, {
        statusOfUpdatingAnItem: {
          isUpdatingAnItem: true,
          wasError: false,
        },
      });

    case UPDATE_ITEM_FROM_SHOPPING_CART:
      if (action.error)
        return Object.assign({}, state, {
          statusOfUpdatingAnItem: {
            isUpdatingAnItem: true,
            wasError: true,
          },
        });
      else {
        return Object.assign({}, state, {
          statusOfAddingAnItem: {
            isAddingAnItem: true,
            wasError: false,
          },
          shoppingCart: Object.assign([], state.shoppingCart, {
            [action.payload.index]: Object.assign({}, state.shoppingCart[action.payload.index], {
                quantity: action.payload.quantity,
            }),
          })
        });
      }
        
    case DID_UPDATE_ITEM_FROM_SHOPPING_CART:
      return Object.assign({}, state, {
        statusOfUpdatingAnItem: {
          isUpdatingAnItem: false,
          wasError: false,
        },
      });
      
    case CLEAN_SHOPPING_CART:
      return Object.assign({}, state, {
        shoppingCart: [],
      });

    default:
      return state;
  }
}
/**
 * It checks if the item specified has been added to shopping cart,
 * if true it returns the object matched else it returns undefined
 */
function thereIsItem(item, state) {
    return state.shoppingCart.find((itemAdded) => item.id == itemAdded.id);
}