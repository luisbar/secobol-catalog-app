/******************
 * Project import *
 ******************/
import {
  WILL_ADD_ITEM_TO_SHOPPING_CART,
  ADD_ITEM_TO_SHOPPING_CART,
  DID_ADD_ITEM_TO_SHOPPING_CART,

  WILL_REMOVE_ITEM_FROM_SHOPPING_CART,
  REMOVE_ITEM_FROM_SHOPPING_CART,
  DID_REMOVE_ITEM_FROM_SHOPPING_CART,
  
  WILL_UPDATE_ITEM_FROM_SHOPPING_CART,
  UPDATE_ITEM_FROM_SHOPPING_CART,
  DID_UPDATE_ITEM_FROM_SHOPPING_CART,
  
  CLEAN_SHOPPING_CART
} from './actionType.js';
/**
 * It adds an item to shopping cart
 */
export function executeAddItem(item) {
  
  return dispatch => {
      
      dispatch(willAddItemToShoppingCart());
      dispatch(addItem(false, item));
      dispatch(didAddItemToShoppingCart());
  }
}
/**
 * It changes a flag for knowing that an item is being added to shopping cart
 */
function willAddItemToShoppingCart() {

  return {
    type: WILL_ADD_ITEM_TO_SHOPPING_CART,
  };
}
/**
 * It changes the shoppingCart object with the data sent or
 * sends an error object when was an error
 */
function addItem(error, payload) {

  return {
    type: ADD_ITEM_TO_SHOPPING_CART,
    payload: payload,
    error: error,
  };
}
/**
 * It restores the deault data about shopping cart
 */
function didAddItemToShoppingCart() {

  return {
    type: DID_ADD_ITEM_TO_SHOPPING_CART,
  };
}
/**
 * It removes an item from shopping cart
 */
export function executeRemoveItem(item) {
  
  return dispatch => {
      
      dispatch(willRemoveItemFromShoppingCart());
      dispatch(removeItem(false, item));
      dispatch(didRemoveItemFromShoppingCart());
  }
}
/**
 * It changes a flag for knowing that an item is being removed from shopping cart
 */
function willRemoveItemFromShoppingCart() {

  return {
    type: WILL_REMOVE_ITEM_FROM_SHOPPING_CART,
  };
}
/**
 * It changes the shoppingCart object with the data sent or
 * sends an error object when was an error
 */
function removeItem(error, payload) {

  return {
    type: REMOVE_ITEM_FROM_SHOPPING_CART,
    payload: payload,
    error: error,
  };
}
/**
 * It restores the deault data about shopping cart
 */
function didRemoveItemFromShoppingCart() {

  return {
    type: DID_REMOVE_ITEM_FROM_SHOPPING_CART,
  };
}
/**
 * It updates an item from shopping cart
 */
export function executeUpdateItem(index, quantity) {
  
  return dispatch => {
      
      dispatch(willUpdateItemFromShoppingCart());
      dispatch(updateItem(false, { index: index, quantity: quantity }));
      dispatch(didUpdateItemFromShoppingCart());
  }
}
/**
 * It changes a flag for knowing that an item is being updated from shopping cart
 */
function willUpdateItemFromShoppingCart() {

  return {
    type: WILL_UPDATE_ITEM_FROM_SHOPPING_CART,
  };
}
/**
 * It changes the shoppingCart object with the data sent or
 * sends an error object when was an error
 */
function updateItem(error, payload) {

  return {
    type: UPDATE_ITEM_FROM_SHOPPING_CART,
    payload: payload,
    error: error,
  };
}
/**
 * It restores the deault data about shopping cart
 */
function didUpdateItemFromShoppingCart() {

  return {
    type: DID_UPDATE_ITEM_FROM_SHOPPING_CART,
  };
}
/**
 * It removes all items of shopping cart
 */
export function cleanShoppingCart() {
  
  return {
    type: CLEAN_SHOPPING_CART,
  };
}