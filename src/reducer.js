/***********************
 * Node modules import *
 ***********************/
import { combineReducers } from 'redux';
/******************
 * Project import *
 ******************/
import dataReducer from './data/reducer.js';

export default combineReducers({
  dataReducer,
});
