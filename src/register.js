/***********************
 * Node modules import *
 ***********************/
import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux';
/******************
 * Project import *
 ******************/
import Store from './store';

import Main from './scene/main';
import Detail from './scene/detail';
import ShoppingCart from './scene/shoppingCart';
import PhotoViewer from './scene/photoViewer';
/**
 * Metodo que registra todas las vistas que manejara la pila de vistas
 */
export function registerScenes() {

  Navigation.registerComponent('secobol.Main', () => Main, Store, Provider);
  Navigation.registerComponent('secobol.Detail', () => Detail, Store, Provider);
  Navigation.registerComponent('secobol.ShoppingCart', () => ShoppingCart, Store, Provider);
  Navigation.registerComponent('secobol.PhotoViewer', () => PhotoViewer);
}
