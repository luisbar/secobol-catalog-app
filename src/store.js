/***********************
 * Node modules import *
 ***********************/
import {
  applyMiddleware,
  createStore,
} from 'redux';
import {
  composeWithDevTools
} from 'remote-redux-devtools';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
/******************
 * Project import *
 ******************/
import rootReducer from './reducer';
/**
 * Settigns of remote-redux-devtools
 * realtime = true, it enables remote-redux-devtools
 * hostname = ip where is running react-native service in order to the device knows where it will send data
 * port = 8000, port o computer
 */
const composeEnhancers = composeWithDevTools({
  realtime: false,
  hostname: '192.168.43.126',
  port: 8000,
});
/**
 * It creates a custom logger
 */
const logger = createLogger({
  collapsed: (getState, action) => !action.error,//Collapsing actions witouth error
  duration: true,
  timestamp: false,
  level: 'info',
  logErrors: false,
});
/**
 * It creates a store
 */
const store = createStore(
  rootReducer,
  composeEnhancers(
    applyMiddleware(thunk, logger)
));

export default store;
