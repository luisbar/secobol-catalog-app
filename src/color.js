export const accentColor = '#C83B0E';
export const darkAccentColor = '#E30613';
export const primaryColor = 'rgba(60, 60, 59, 1)';
export const textColor = '#FFFFFF';
export const backgroundColor = '#FFFFFF';
export const cowBorderColor = '#FFFFFF';
export const transparent = 'rgba(0, 0, 0, 0)';

export const dangerColor = '#FF8A65';
export const successColor = '#AED581';
