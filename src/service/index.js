/***********************
 * Node modules import *
 ***********************/
import Realm from 'realm';
import Firebase from 'react-native-firebase';
import RNFetchBlob from 'react-native-fetch-blob';
/******************
 * Project import *
 ******************/
import Cut from './realm/cut.js';
import Usage from './realm/usage.js';
import RealTimeDatabase from './realTimeDatabase/index.js';
import FileManager from './fileManager/index.js';
/*************
 * Constants *
 *************/
const cut = new Cut();
const usage = new Usage();
const firebase = new Firebase();

export const realm = new Realm(
{ schema:
  [
    cut,
    usage,
  ],
});

export const realTimeDatabase = new RealTimeDatabase(firebase.database());
export const fileManager = new FileManager(RNFetchBlob);
