/***********************
 * Node modules import *
 ***********************/
import {
  fileManager
} from '../index.js';
/**
 * It communicates with real time database of firebase
 */
export default class RealTimeDatabase {

  constructor(realTimeDatabase) {
    this.realTimeDatabase = realTimeDatabase;
  }
  /**
   * It fetchs cuts of beef from firebase
   */
  getCuts(successCb, errorCb) {
    this.realTimeDatabase
    .ref('cuts')
    .once('value')
    .then((snapshot) => fileManager.saveImages(snapshot.val(), successCb, errorCb))
    .catch((error) => errorCb(error));
  }
}
