export default class Usage {

  constructor() {
    this.name = 'Usage';
    this.properties = {
      value: { type: 'string' },
    };
  }
}
