export default class Cut {

  constructor() {
    this.name = 'Cut';
    this.primaryKey = 'id';
    this.properties = {
      id: { type: 'string' },
      name: { type: 'string' },
      description: { type: 'string', optional: true },
      price: { type: 'float' },
      mainImage: { type: 'string' },
      mainImageName: { type: 'string' },
      secondaryImage: { type: 'string', optional: true },
      secondaryImageName: { type: 'string', optional: true },
      number: { type: 'int', optional: true },
      subCuts: { type: 'list', objectType: 'Cut', },
      usages: { type: 'list', objectType: 'Usage' },
    };
  }
}
