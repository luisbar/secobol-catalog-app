/**
 * It has methods for managing files
 */
export default class FileManager {

  constructor(fileManager) {
    this.fileManager = fileManager;
    this.dirs = fileManager.fs.dirs;
  }
  /**
   * It save images about cuts of beef in local
   */
  saveImages(cuts, successCb, errorCb) {
    let promises = [];

    Object.keys(cuts).map((key) => {
      //To save the main image
      promises.push(this.saveImage(cuts[key].mainImageName, cuts[key].mainImage));
      //To save the secondary image
      promises.push(this.saveImage(cuts[key].secondaryImageName, cuts[key].secondaryImage));
      //To save the main image about sub cuts
      if (cuts[key].subCuts)
        Object.keys(cuts[key].subCuts).map((subCutKey) => {
          
          promises.push(this.saveImage(
            cuts[key].subCuts[subCutKey].mainImageName,
            cuts[key].subCuts[subCutKey].mainImage
          ));
        })
    })
    
    Promise.all(promises)
    .then((response) => successCb(cuts))
    .catch((error) => cbError(error))
  }
  /**
   * It saves an image
   */
  saveImage(imageName, image, errorCb) {
    return this.fileManager.config({
        path : this.dirs.DocumentDir + '/images/' + imageName,
      })
      .fetch('GET', image);
  }
}
