import { dangerColor, primaryColor } from '../../../../color';
/**
 * Esta hoja de estilo recibe 4 parametros por que el inputWithValidator
 * tiene un componente icon y un text que debe modificarse dinamicamente
 */
export default function(iconOpacity, iconColor, inputTextColor, labelTextColor) {

  return {

    item: {
      marginBottom: 10,
      backgroundColor: primaryColor,
      borderColor: primaryColor,
      marginTop: 10,
    },
    label: {
      color: labelTextColor,
      fontFamily: 'Nobel-Light',
      fontSize: 14,
    },
    icon: {
      opacity: iconOpacity,
      color: iconColor,
    },
    input: {
      color: inputTextColor,
      minHeight: 60,
      fontFamily: 'Nobel-Light',
      fontSize: 14,
    },
    multilineInput: {
      color: inputTextColor,
      minHeight: 100,
    },
    advice: {
      position: 'absolute',
      color: dangerColor,
      top: 40,
    },
  };
}
