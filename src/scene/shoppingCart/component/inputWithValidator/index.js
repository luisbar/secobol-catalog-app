/***********************
 * Node modules import *
 ***********************/
import React, { Component } from 'react';

import {
  Item,
  Label,
  Icon,
  Input,
  Text,
} from 'native-base';

import isEmail from 'validator/lib/isEmail';
import isNumeric from 'validator/lib/isNumeric';
/******************
 * Project import *
 ******************/
import {
  successColor,
  dangerColor,
  textColor,
} from '../../../../color';

import style from './style';
/**
 * keyboardTypes
 */
const EMAIL_ADDRESS = 'email-address';
const PASSWORD = 'password';
const PHONE = 'phone-pad';
const DEFAULT = 'default';
const NUMERIC = 'numeric';
/**
 * Componente input modificado de native base que valida el texto introducido,
 * de acuerdo al keyboardType, y envia su valor y el resuldato de la validacion
 * de acuerdo al keyboardType
 */
export default class InputWithValidator extends Component {

  constructor(props) {
    super(props);
    this.state = {
      icon: {
        android: 'md-close-circle',
        ios: 'ios-close-circle',
      },
      iconOpacity: 0,
      iconColor: dangerColor,
      advice: undefined,
    };
  }

  render() {

    return (
      <Item
        last
        style={style().item}
        onPress={this.props.onPress}>

        <Label style={style(this.state.iconOpacity,
          this.state.iconColor,
          this.props.inputTextColor,
          this.props.labelTextColor).label}>{this.props.label}
        </Label>

        <Input
          ref={'input'}
          style={this.props.multiline
            ? style(this.state.iconOpacity,
              this.state.iconColor,
              this.props.inputTextColor,
              this.props.labelTextColor).multilineInput
            : style(this.state.iconOpacity,
              this.state.iconColor,
              this.props.inputTextColor,
              this.props.labelTextColor).input}
          placeholderTextColor={this.props.placeholderTextColor
            ? this.props.placeholderTextColor
            : textColor}
          placeholder={this.props.placeholder}
          keyboardType={this.props.keyboardType}
          secureTextEntry={this.props.secureTextEntry}
          maxLength={this.props.maxLength}
          value={this.props.value}
          multiline={this.props.multiline}
          editable={this.props.onPress ? false : true}
          returnKeyType={this.props.returnKeyType || 'next'}
          onSubmitEditing={this.props.onSubmitEditing}
          autoFocus={this.props.autoFocus}
          onChangeText={(value) =>
            this.handleInputValue(value)
          }/>
        <Icon
          android={this.state.icon.android} ios={this.state.icon.ios}
          style={style(this.state.iconOpacity, this.state.iconColor).icon}/>
        <Text style={style().advice}>{this.state.advice}</Text>
      </Item>
    );
  }
  /**
   * Maneja el evento onChangeText, y dispara el escuchador
   * onHandleInputValue enviado como props
   */
  handleInputValue(value) {
    const isValid = this.checkKeyboardType(value);

    this.updateState(isValid);
    this.props.onHandleInputValue(this.props.id, isValid, value);
  }
  /**
   * Verifica si el texto esta correcto de acuerdo al keyboardType
   */
  checkKeyboardType(value) {

    if (this.props.minLength !== -1) {//Requerido
      switch (this.props.keyboardType) {
        case EMAIL_ADDRESS:
          return value && isEmail(value);

        case PASSWORD:
          return value &&
                  value.length >= this.props.minLength &&
                  /([a-z]+\d+)|(\d+[a-z]+)/ig.test(value);

        case NUMERIC:
          return value && value.length >= this.props.minLength && isNumeric(value);

        case PHONE:
        case DEFAULT:
          return value && value.length >= this.props.minLength;
      }
    } else
      return true;
  }
  /**
   * Retorna un mensaje de error si el texto introducido es
   * erroneo, el mensaje es de acuerdo al keyboardType
   */
  getErrorMessage() {

    switch (this.props.keyboardType) {
      case EMAIL_ADDRESS:
        return 'Correo invalido';

      case PASSWORD:
        return 'Clave corta o no es alfanumerica';

      case PHONE:
        return 'Telefono invalido';

      case NUMERIC:
        return 'Numero invalido';

      case DEFAULT:
        return 'Texto muy corto';
    }
  }
  /**
   * Actualiza el estado del componente de acuerdo al parametro
   * isValid, pasado por parametro
   */
  updateState(isValid) {

    this.setState({
      icon: {
        android: isValid ? 'md-checkmark-circle-outline' : 'md-close-circle',
        ios: isValid ? 'ios-checkmark-circle-outline' : 'ios-close-circle',
      },
      iconOpacity: 1,
      iconColor: isValid ? successColor : dangerColor,
      advice: isValid ? undefined : this.getErrorMessage(),
    });
  }
}
