/******************
 * Project import *
 ******************/
import {
  primaryColor,
  accentColor,
  textColor,
} from '../../color.js';
export default function (height) {
  
  return {
    
    container: {
      backgroundColor: primaryColor
    },
    leftHeader: {
      maxWidth: 30,
    },
    headerTitle: {
      color: accentColor,
      fontFamily: 'Nobel-Bold',
      fontSize: 14,
      alignSelf: 'center',
    },
    secondaryContainer: {
      backgroundColor: primaryColor,
      margin: 10,
    },
    mailInputContainer: {
      borderStyle: 'dashed',
      borderColor: textColor,
      borderWidth: 2,
      height: 85,
    },
    cutListContainer: {
      borderStyle: 'dashed',
      borderColor: textColor,
      borderWidth: 2,
      marginTop: 10,
      height: height - 179,
    },
    headerGrid: {
      maxHeight: 25,
    },
    headerCol: {
      maxHeight: 25,
    },
    listHeaderText: {
      fontFamily: 'Nobel-Light',
      color: textColor,
      fontSize: 14,
    },
    paddingLeftlistHeaderText: {
      paddingLeft: 2,
    },
    contentCol: {
      justifyContent: 'flex-end',
    },
    listContentText: {
      fontFamily: 'Nobel-Light',
      color: textColor,
      fontSize: 14,
    },
    listContentTextAccentColor: {
      fontFamily: 'Nobel-Light',
      color: accentColor,
      fontSize: 14,
    },
    paddingLeftlistContentText: {
      paddingLeft: 2,
    },
    quantityItem: {
      maxHeight: 25,
    },
    deleteIconContainer: {
      alignItems: 'flex-end',
    },
    deleteIcon: {
      paddingRight: 2,
    },
  };
}