/***********************
 * Node modules import *
 ***********************/
import React from 'react';

import {
  Container,
  Content,
  Header,
  Right,
  StyleProvider,
  Title,
  List,
  Text,
  Left,
  Body,
  Item,
  Input,
  Grid,
  Col
} from 'native-base';

import {
  View
} from 'react-native';

import Mailer from 'react-native-mail';

import MIIcon from 'react-native-vector-icons/MaterialIcons';
import IIcon from 'react-native-vector-icons/Ionicons';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
/******************
 * Project import *
 ******************/
import {
  accentColor,
  textColor,
} from '../../color.js';

import BasisComponent from '../basisComponent.js';

import getTheme from '../../theme/components';
import material from '../../theme/variables/material';

import InputWithValidator from './component/inputWithValidator/index.js';

import style from './style.js';

import {
  executeAddItem,
  executeRemoveItem,
  executeUpdateItem,
} from '../../data/shoppingCart/action.js';
/**
 * It shows the cart shopping view, and it view manages the cart shopping content
 */
class ShoppingCart extends BasisComponent {
  
  constructor(props) {
    super(props);
  
    this.state = {
      cartItems: [],
      emailInputValue: null,
      emailInputState: false,
    };
  }
  
  componentWillMount() {
    this.props.executeAddItem({
      id: this.props.id,
      name: this.props.name,
      price: (this.props.price).toFixed(2),
      quantity: 1,
    })
  }
  
  render() {

    return (
      <StyleProvider style={getTheme(material)}>
        <Container
          style={style().container}>
          
          {
            this.renderHeader()
          }
          
          <Content
            style={style().secondaryContainer}>

            <View
              style={style().mailInputContainer}>

              <InputWithValidator
                id={'EMAIL'}
                ref={'EMAIL'}
                keyboardType={'email-address'}
                placeholder={'Escriba aquí'}
                placeholderTextColor={textColor}
                label={'Correo:'}
                labelTextColor={textColor}
                inputTextColor={textColor}
                minLength={10}
                maxLength={100}
                onHandleInputValue={this._onHandleInputValue.bind(this)}/>
            </View>
            
            <View
              style={style(this.getHeight()).cutListContainer}>
              
              {
                this.renderListHeader()
              }
              
              <List
                dataArray={this.props.shoppingCart}
                renderRow={(item) => this._renderRow(item)}/>
            </View>
          </Content>
        </Container>
      </StyleProvider>
    );
  }
  /**
   * It renders the header of cart shopping view
   */
  renderHeader() {
    
    return (
      <Header>
        <Left
          style={style().leftHeader}>
          <MIIcon
            name={'close'}
            size={30}
            color={accentColor}
            onPress={() => this.props.navigator.dismissModal()}/>
        </Left>

        <Title style={style().headerTitle}>{'CARRITO'}</Title>

        <Right>
          <MIIcon
            name={'send'}
            size={30}
            color={accentColor}
            onPress={() => this.sendEmail()}/>
        </Right>
      </Header>
    );
  }
  /**
   * It renders a header for the cut list
   */
  renderListHeader() {
    
    return (
      <Grid
        style={style().headerGrid}>
        <Col
          style={style().headerCol}>
          <Text style={[style().listHeaderText, style().paddingLeftlistHeaderText]}>{'Corte'}</Text>
        </Col>
      
        <Col
          style={style().headerCol}>
          <Text style={style().listHeaderText}>{'Precio'}</Text>
        </Col>
      
        <Col
          style={style().headerCol}>
          <Text style={style().listHeaderText}>{'Cantidad'}</Text>
        </Col>
      
        <Col
          style={style().headerCol}>
          <Text style={style().listHeaderText}>{'Total'}</Text>
        </Col>
    
        <Col
          style={style().headerCol}>
        </Col>
      </Grid>
    )
  }
  /**
   * It renders a row for showing the content about list of cuts added
   */
  _renderRow(item) {
    
    return (
      <Grid>
        <Col
          style={style().contentCol}>
            <Text
              style={[style().listContentText, style().paddingLeftlistContentText]}
              numberOfLines={1}>
              {item.name.toLowerCase()}
            </Text>
        </Col>
        
        <Col
          style={style().contentCol}>
            <Text
              style={style().listContentText}
              numberOfLines={1}>
              {item.price}
            </Text>
        </Col>

        <Col
          style={style().contentCol}>
            <Item
              style={style().quantityItem}>
              <Input
                keyboardType={'numeric'}
                style={style().listContentText}
                value={item.quantity.toString()}
                maxLength={4}
                onChangeText={(value) =>
                  this.props.executeUpdateItem(this.getItemPosition(item), value)
                }/>
            </Item>
        </Col>
      
        <Col
          style={style().contentCol}>
          <Text
            style={style().listContentText}
            numberOfLines={1}>
            {parseFloat(item.price) * parseFloat(item.quantity ? item.quantity : 0)}
          </Text>
        </Col>
      
        <Col
          style={style().deleteIconContainer}>
          <IIcon
            name={'ios-trash'}
            size={25}
            color={accentColor}
            style={style().deleteIcon}
            onPress={() => this.props.executeRemoveItem(item)}/>
        </Col>
      </Grid>
    )
  } 
  /**
   * It checks if the item specified has been added to shopping cart,
   * if true it returns the position else it returns undefined
   */
  getItemPosition(item) {
      return this.props.shoppingCart.findIndex((itemAdded) => item.name == itemAdded.name);
  }
  /**
   * It sends an email with all cuts added
   */
  sendEmail() {
    if (this.state.emailInputState && !this.thereIsAZeroQuantity()) {
      const _this = this;
      const body = this.buildBody();
      
      Mailer.mail({
        subject: 'Proforma de cortes de carne Secobol',
        recipients: [this.state.emailInputValue],
        ccRecipients: ['clavijoalan@gmail.com'],
        body: body,
        isHTML: true,
      }, (error, event) => {
        if (error)
          _this.showErrorMessage('Necesita configurar la aplicación de correo');
      });
    } else {
      this.refs.EMAIL.handleInputValue(this.state.emailInputValue);
      this.showErrorMessage('Falta introducir el correo o la cantidad de algun corte es 0, o el campo esta vacio');
    }
  }
  /**
   * It is a listener that listens when the mail input content is changed
   */
  _onHandleInputValue(id, state, value) {
      this.setState({
        emailInputState: state,
        emailInputValue: value,
      })
  }
  /**
   * Listener inherited of its father
   */
  onChageOrientation() {
    this.forceUpdate();
  }
  /**
   * It checks if a quantity of an item at shopping cart
   * is zero
   */
  thereIsAZeroQuantity() {
    return this.props.shoppingCart.find((item) => item.quantity == 0 || item.quantity == '0')
  }
  /**
   * It returns a html view for sending it by email
   */
  buildBody() {
    
    let elements = '';
    
    this.props.shoppingCart.map((item) => {
        elements = elements +
            `<tr>
                <td>${item.name}</td>
                <td style='text-align: center;'>${item.price}</td>
                <td style='text-align: center;'>${item.quantity}</td>
                <td style='text-align: center;'>${parseInt(item.price) * parseInt(item.quantity)}</td>
             </tr>`
    });
    
    return `<table style="width:100%">
              <tr>
                <th>Corte</th>
                <th>Precio</th> 
                <th>Cantidad</th>
                <th>Total</th>
              </tr>
              ${elements}
            </table>`
  }
}

const mapStateToProps = state => ({
  shoppingCart: state.dataReducer.shoppingCartReducer.shoppingCart,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  executeAddItem: executeAddItem,
  executeRemoveItem: executeRemoveItem,
  executeUpdateItem: executeUpdateItem,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ShoppingCart);
