/***********************
 * Node modules import *
 ***********************/
import React from 'react';

import {
  Image,
} from 'react-native';

import {
  Body,
} from 'native-base';
/******************
 * Project import *
 ******************/
import style from './style.js';

import BasisComponent from '../../../basisComponent.js';
/**
 * It renders icons about usages from an array received,
 * they are render in a container with row direction
 */
class Usage extends BasisComponent {

  constructor(props) {
    super(props);
  }

  render() {

    return (
      <Body
        style={style(this.getWidth()).container}>
        {
          this.props.usages.map((item, index) =>
            <Image
              key={index}
              style={style().imageOfUsage}
              resizeMode={'contain'}
              source={this.getImageOfUsage(item.value)} />
          )
        }
      </Body>
    );
  }
  /**
   * It returns image of a type of usage
   */
  getImageOfUsage(usage) {

    switch (usage) {

      case 'pot':
        return require('../../../../image/olla/olla.png');

      case 'tritured':
        return require('../../../../image/molida/molida.png');

      case 'oven':
        return require('../../../../image/horno/horno.png');

      case 'grill':
        return require('../../../../image/parrilla/parrilla.png');

      case 'griddle':
        return require('../../../../image/plancha/plancha.png');

      case 'skillet':
        return require('../../../../image/sarten/sarten.png');
    }
  }
}

export default Usage;
