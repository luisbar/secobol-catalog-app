export default function (width) {
  
  return {
    
    container: {
      flexDirection: 'row',
      justifyContent: 'center',
      width: width
    },
    imageOfUsage: {
      height: 30,
      width: 30,
      marginLeft: 10,
    },
  };
}