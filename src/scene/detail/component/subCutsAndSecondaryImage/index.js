/***********************
 * Node modules import *
 ***********************/
import React from 'react';

import {
  Image,
} from 'react-native';

import {
  Grid,
  Col,
  Text,
  Body
} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
/******************
 * Project import *
 ******************/

import BasisComponent from '../../../basisComponent.js';

import style from './style.js';

import {
  fileManager
} from '../../../../service/index.js';

import {
  accentColor
} from '../../../../color.js';
/**
 * It renders the sub cuts about the cut of beef and a secondary
 * image about a cut
 */
class SubCutsAndSecondaryImage extends BasisComponent {

  constructor(props) {
    super(props);

    this.state = {
      images: [],
    }
  }

  componentWillMount() {
    Object.keys(this.props.subCuts).map((key) => {
      this.loadImageFromDevice(this.props.subCuts[key].mainImageName)
    })
  }

  render() {

    return (
      <Grid>
        {
          this.props.subCuts.length != 0 &&
          <Col
            style={style().leftCol}>
            {
              this.props.subCuts.map((item, index) =>

                <Body
                  key={index}
                  style={style(this.getWidth()).itemContainer}>

                  <Text
                    style={this.props.orientation == 'portrait'
                      ? style().subCutItem
                      : style().landscapeSubCutItem }
                    onPress={() => this.goPhotoViewer(this.state.images[index], item.description)}
                    numberOfLines={1}>
                    {item.name.toUpperCase()}
                  </Text>

                  <Icon
                    style={style().icon}
                    name={'cart-plus'}
                    size={25}
                    color={accentColor}
                    onPress={() => this.goShoppingCart(item)}/>
                </Body>
              )
            }
          </Col>
        }

        <Col>
          <Image
            style={style(this.props.subCuts.length != 0
              ? this.getWidth() / 2
              : this.getWidth(), this.getWidth() / 2).cowImage}
            source={{ uri: this.props.secondaryImage }}
            resizeMode={'contain'}/>
        </Col>
      </Grid>
    );
  }
  /**
   * It reads the main image of a sub cut from device memory
   * and save it in the state
   */
  loadImageFromDevice(mainImageName) {
    fileManager.fileManager.fs.readFile(fileManager.dirs.DocumentDir + '/images/' + mainImageName, 'base64')
    .then((data) => {
      let aux = this.state.images;
      aux.push('data:image/png;base64,' + data);

      this.setState({ mainImage:  aux })
    });
  }
  /**
   * It shows the photo viewer view
   */
  goPhotoViewer(image, description) {
    this.props.navigator.showModal({
      screen: 'secobol.PhotoViewer',
      passProps: {
        image: image,
        description: description,
      },
      navigatorStyle: {
        navBarHidden: true,
      },
    });
  }
  /**
   * It shows the shopping cart view
   */
  goShoppingCart(cutOfBeef) {
    this.props.navigator.showModal({
      screen: 'secobol.ShoppingCart',
      passProps: {
        id: cutOfBeef.id,
        name: cutOfBeef.name,
        price: cutOfBeef.price,
      },
      navigatorStyle: {
        navBarHidden: true,
      },
    });
  }
}

export default SubCutsAndSecondaryImage;
