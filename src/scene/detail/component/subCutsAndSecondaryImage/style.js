/******************
 * Project import *
 ******************/
import {
  textColor,
} from '../../../../color.js';

export default function (width, height) {

  return {
    leftCol: {
      justifyContent: 'center',
    },
    subCutItem: {
      fontFamily: 'Nobel-Light',
      color: textColor,
      fontSize: 14,
      marginLeft: 10,
      marginTop: 10,
      paddingBottom: 8,
      flex: 1,
    },
    landscapeSubCutItem: {
      fontFamily: 'Nobel-Light',
      color: textColor,
      fontSize: 14,
      marginLeft: 10,
      marginTop: 10,
      paddingBottom: 8,
      flex: 1,
    },
    cowImage: {
      width: width,
      height: height,
    },
    itemContainer: {
      width: width / 2,
      flexDirection: 'row',
      alignItems: 'center'
    },
    icon: {
      flex: 1,
    }
  };
}
