/******************
 * Project import *
 ******************/
import {
  primaryColor,
  accentColor,
  textColor,
  transparent
} from '../../color.js';

export default function(width, height) {

  return {

    container: {
      backgroundColor: primaryColor,
    },
    landscapeCloseButton: {
      maxWidth: 30
    },
    titleContainer: {
      flexDirection: 'row',
      justifyContent: 'flex-start',
    },
    landscapeNumberContainer: {
      borderRadius: 10,
      backgroundColor: accentColor,
      width: 20,
      height: 20,
      overflow: 'hidden',
      alignItems: 'center',
      justifyContent: 'center',
      alignSelf: 'center',
    },
    landscapeNumber: {
      color: textColor,
    },
    landscapeTitle: {
      color: accentColor,
      alignSelf: 'center',
      marginLeft: 5,
      fontFamily: 'Nobel-Bold',
      fontSize: 14,
    },
    cutAndMethodContainer: {
      width: width,
      height: height / 1.5,
      flexDirection: 'row',
    },
    landscapeCowImage: {
      width: width / 2,
      height: height / 1.5,
    },
    description: {
      color: textColor,
      textAlign: 'justify',
      padding: 10,
      fontFamily: 'Nobel-Light'
    },
    landscapeDescription: {
      color: textColor,
      textAlign: 'justify',
      padding: 10,
      fontFamily: 'Nobel-Light',
      width: width / 2
    },
  };
}
