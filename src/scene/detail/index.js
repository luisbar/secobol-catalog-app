/***********************
 * Node modules import *
 ***********************/
import React from 'react';

import {
  Image,
  View
} from 'react-native';

import {
  Container,
  Content,
  Text,
  Title,
  Body,
  Right,
  Header,
  Button,
  StyleProvider,
  Left
} from 'native-base';

import FAIcon from 'react-native-vector-icons/FontAwesome';
import MIIcon from 'react-native-vector-icons/MaterialIcons';

import BasisComponent from '../basisComponent.js';

import CollapsingToolbar from 'react-native-collapsingtoolbar';

import { connect } from 'react-redux';

import {
  fileManager
} from '../../service/index.js';
/******************
 * Project import *
 ******************/
import {
  primaryColor,
  accentColor,
} from '../../color.js';

import style from './style.js';

import SubCutsAndSecondaryImage from './component/subCutsAndSecondaryImage/index.js';
import Usage from './component/usage/index.js';

import getTheme from '../../theme/components';
import material from '../../theme/variables/material';
/**
 * It render a view for showing a cut of beef, its description and its usage according name
 */
class Detail extends BasisComponent {

  constructor(props) {
    super(props);

    this.state = {
      orientation: this.getOrientation(),//orientation for passing its children
      mainImage: undefined,
      secondaryImage: undefined,
    };
  }

  render() {

    return (
      <StyleProvider style={getTheme(material)}>
        {
          this.getOrientation() == 'portrait'
          ? this.renderPortraitView(this.getCutOfBeef())
          : this.renderLandscapeView(this.getCutOfBeef())
        }
      </StyleProvider>
    );
  }

  componentDidMount() {
    const cutOfBeef = this.getCutOfBeef();
    this.loadImagesFromDevice(cutOfBeef.mainImageName, cutOfBeef.secondaryImageName);
  }
  /**
   * It renders a view for portrait orientation
   */
  renderPortraitView(cutOfBeef) {

    return (
      <Container
        style={style().container}>
        <CollapsingToolbar
          number={cutOfBeef.number}
          toolbarMaxHeight={this.getHeight() / 2.5}
          toolbarColor={primaryColor}
          title={cutOfBeef.name.toUpperCase()}
          titleColor={accentColor}
          src={{ uri: this.state.mainImage }}
          leftItem={<MIIcon name={'close'} size={30} color={accentColor}/>}
          leftItemPress={() => this.props.navigator.pop()}
          rightItem={<FAIcon name={'cart-plus'} size={30} color={accentColor}/>}
          rightItemPress={() => this.goShoppingCart(cutOfBeef)}>

          <Content
            style={style().container}>

            <Text style={style(this.getWidth()).description}>{cutOfBeef.description}</Text>

            <SubCutsAndSecondaryImage
              secondaryImage={this.state.secondaryImage}
              subCuts={cutOfBeef.subCuts}
              orientation={this.state.orientation}
              navigator={this.props.navigator}/>

            <Usage
              usages={cutOfBeef.usages}/>
          </Content>
        </CollapsingToolbar>
      </Container>
    );
  }
  /**
   * It renders a view for landscape orientation
   */
  renderLandscapeView(cutOfBeef) {

    return (
      <Container
        style={style().container}>
        {
          this.renderLandscapeHeader(cutOfBeef)
        }
        <Content
          style={style().container}>

          <Body
            style={style(this.getWidth(), this.getHeight()).cutAndMethodContainer}>

            <Image
              resizeMode={'stretch'}
              style={style(this.getWidth(), this.getHeight()).landscapeCowImage}
              source={{ uri: this.state.mainImage }}/>

            <Text style={style(this.getWidth()).landscapeDescription}>{cutOfBeef.description}</Text>
          </Body>


          <SubCutsAndSecondaryImage
            secondaryImage={this.state.secondaryImage}
            subCuts={cutOfBeef.subCuts}
            orientation={this.state.orientation}
            navigator={this.props.navigator}/>

          <Usage
            usages={cutOfBeef.usages}/>
        </Content>
      </Container>
    );
  }
  /**
   * It renders an header for landscape view
   */
  renderLandscapeHeader(cutOfBeef) {

    return (
      <Header>
        <Left
          style={style().landscapeCloseButton}>
          <MIIcon
            name={'close'}
            size={30}
            color={accentColor}
            onPress={() => this.props.navigator.pop()}/>
        </Left>

        <Body
          style={style().titleContainer}>
          <View style={style().landscapeNumberContainer}>
            <Text style={style().landscapeNumber}>{cutOfBeef.number}</Text>
          </View>
          <Title style={style().landscapeTitle}>{cutOfBeef.name.toUpperCase()}</Title>
        </Body>

        <Right>
          <FAIcon
            name={'cart-plus'}
            size={30}
            color={accentColor}
            onPress={() => this.goShoppingCart(cutOfBeef)}/>
        </Right>
      </Header>
    );
  }
  /**
   * It gets a specific cut of beef by its number
   */
  getCutOfBeef() {
    return this.props.cuts.filter((item) => item.number == this.props.number)[0];
  }
  /**
   * Listener inherited of its father
   */
  onChageOrientation() {
    this.setState({
      orientation: this.getOrientation(),//orientation for passing its children
    });
  }
  /**
   * It shows the shopping cart view
   */
  goShoppingCart(cutOfBeef) {
    this.props.navigator.showModal({
      screen: 'secobol.ShoppingCart',
      passProps: {
        id: cutOfBeef.id,
        name: cutOfBeef.name,
        price: cutOfBeef.price,
      },
      navigatorStyle: {
        navBarHidden: true,
      },
    });
  }
  /**
   * It reads the main image and secondary image from device memory
   */
  loadImagesFromDevice(mainImageName, secondaryImageName) {
    fileManager.fileManager.fs.readFile(fileManager.dirs.DocumentDir + '/images/' + mainImageName, 'base64')
    .then((data) => this.setState({ mainImage: 'data:image/png;base64,' + data }));

    fileManager.fileManager.fs.readFile(fileManager.dirs.DocumentDir + '/images/' + secondaryImageName, 'base64')
    .then((data) => this.setState({ secondaryImage: 'data:image/png;base64,' + data }));
  }
}

const mapStateToProps = state => ({
  cuts: state.dataReducer.cutReducer.cuts,
});

export default connect(
  mapStateToProps,
)(Detail);
