/******************
 * Project import *
 ******************/
import {
  primaryColor,
  textColor,
} from '../../color.js';

export default function (width, height) {

  return {

    container: {
      backgroundColor: primaryColor,
    },
    image: {
      width: width,
      height: height / 2,
    },
    leftHeader: {
      maxWidth: 30,
    },
    description: {
      color: textColor,
      textAlign: 'justify',
      padding: 10,
      fontFamily: 'Nobel-Light'
    },
  };
}
