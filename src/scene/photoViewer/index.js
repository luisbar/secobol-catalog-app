/***********************
 * Node modules import *
 ***********************/
import React from 'react';

import {
  Image
} from 'react-native';

import {
  Container,
  Content,
  Header,
  Left,
  StyleProvider,
  Right,
  Body,
  Text,
} from 'native-base';

import Icon from 'react-native-vector-icons/MaterialIcons';
/******************
 * Project import *
 ******************/
import BasisComponent from '../basisComponent.js';

import {
  accentColor
} from '../../color.js';

import getTheme from '../../theme/components';
import material from '../../theme/variables/material';

import style from './style.js';
/**
 * It shows an image sent by props
 */
class PhotoViewer extends BasisComponent {

  render() {

    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          <Header>
            <Left
              style={style().leftHeader}>
              <Icon
                name={'close'}
                size={30}
                color={accentColor}
                onPress={() => this.props.navigator.dismissModal()}/>
            </Left>
            
            <Body/>
            <Right/>
          </Header>

          <Content
            style={style().container}>

            <Image
              style={style(this.getWidth(), this.getHeight()).image}
              resizeMode={'cover'}
              source={{ uri: this.props.image }}/>
              
            <Text style={style().description}>{this.props.description}</Text>
          </Content>
        </Container>
      </StyleProvider>
    );
  }

  onChageOrientation() {
    this.forceUpdate()
  }
}

export default PhotoViewer;
