/******************
 * Project import *
 ******************/
import {
  primaryColor,
} from '../../color.js';

export default {

  container: {
    backgroundColor: primaryColor,
  },
};
