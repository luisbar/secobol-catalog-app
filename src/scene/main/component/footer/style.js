/******************
 * Project import *
 ******************/
import {
  textColor,
} from '../../../../color.js';

export default function (width) {

  return {
    item: {
      flexDirection: 'row',
      width: width / 2,
      marginBottom: 5,
      marginLeft: 20,
    },
    numberContainer: {
      borderWidth: 1,
      borderColor: textColor,
      borderRadius: 10,
      width: 20,
      height: 20,
      justifyContent: 'center',
      alignItems: 'center',
    },
    number: {
      color: textColor,
      fontSize: 14,
      fontFamily: 'Nobel-Light',
    },
    landscapeNumber: {
      color: textColor,
      fontSize: 14,
      fontFamily: 'Nobel-Light',
    },
    text: {
      color: textColor,
      fontSize: 14,
      fontFamily: 'Nobel-Light',
      marginLeft: 10,
      marginRight: 10,
      flex: 1,
    },
    landscapeText: {
      color: textColor,
      fontSize: 14,
      fontFamily: 'Nobel-Light',
      marginLeft: 10,
      marginRight: 10,
      flex: 1,
    },
  };
};
