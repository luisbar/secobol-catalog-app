/***********************
 * Node modules import *
 ***********************/
import React from 'react';

import {
  Grid,
  Col,
  Body,
  Text
} from 'native-base';

import {
  View
} from 'react-native';
/******************
 * Project import *
 ******************/
import style from './style.js';

import BasisComponent from '../../../basisComponent.js';
/**
 * It renders a footer for the main scene
 */
class Footer extends BasisComponent {

  constructor(props) {
    super(props);
  }

  render() {

    return (
      <Grid>
        <Col>
          {
            this.renderCutOfBeef(this.props.cuts, 1, 16)
          }
        </Col>

        <Col>
          {
            this.renderCutOfBeef(this.props.cuts, 17, 32)
          }
        </Col>
      </Grid>
    );
  }
  /**
   * It renders names of cut of beef with a number beside of them
   */
  renderCutOfBeef(list, start, end) {

    return (

      list.map((item, index) => {
          if (item.number >= start && item.number <= end)
            return this.getItemView(item, index);
        }
      )
    );
  }
  /**
   * It returs a view for rendering an item of a list of cuts
   */
  getItemView(item, index) {

    return (

      <Body
        key={index}
        style={style(this.getWidth()).item}>

        <View
          style={style().numberContainer}
          onPress={() => this.props.onOpenDetail(item.number)}>

          <Text
            style={this.props.orientation == 'portrait' ? style().number : style().landscapeNumber}
            onPress={() => this.props.onOpenDetail(item.number)}>
            {item.number}
          </Text>
        </View>

        <Text
          style={this.props.orientation == 'portrait' ? style().text : style().landscapeText}
          onPress={() => this.props.onOpenDetail(item.number)}
          numberOfLines={1}>
          {item.name.toUpperCase()}
        </Text>
      </Body>
    );
  }
}

export default Footer;
