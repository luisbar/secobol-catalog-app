/***********************
 * Node modules import *
 ***********************/
import React from 'react';

import {
  Body,
  Spinner,
  Text,
} from 'native-base';
/******************
 * Project import *
 ******************/
import BasisComponent from '../../../basisComponent.js'

import {
  accentColor,
  primaryColor,
  textColor
} from '../../../../color.js';

import style from './style.js';
/**
 * It renders a component composed by an spinner and a text for showing
 * the progress about a task
 */
class Loading extends BasisComponent {
  
  constructor(props) {
    super(props);
  }
  
  render() {

    return (
      <Body
        style={style(this.getWidth(), this.getHeight()).container}>
      
        <Spinner color={accentColor}/>
        <Text style={style().text}>{'Cargando...'}</Text>
      </Body>
    );
  }
}

export default Loading;