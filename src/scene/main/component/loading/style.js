/******************
 * Project import *
 ******************/
import {
  primaryColor,
  textColor
} from '../../../../color.js';
 
export default function(width, height) {
  
  return {
    
    container: {
      width: width,
      height: height,
      backgroundColor: primaryColor,
      justifyContent: 'center',
      alignItems: 'center'
    },
    text: {
      color: textColor
    }
  };
}