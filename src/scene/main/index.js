/***********************
 * Node modules import *
 ***********************/
import React from 'react';

import {
  Container,
  Content,
  StyleProvider,
} from 'native-base';

import CollapsingToolbar from 'react-native-collapsingtoolbar';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/Ionicons';
/******************
 * Project import *
 ******************/
import BasisComponent from '../basisComponent.js';
import Detail from '../detail/index.js';

import getTheme from '../../theme/components';
import material from '../../theme/variables/material';
import style from './style.js';
import Footer from './component/footer';
import Cow from './component/cow';
import Loading from './component/loading';

import {
  primaryColor,
  accentColor,
} from '../../color.js';

import {
  executeGetCuts,
} from '../../data/cut/action.js';
/**
 * It renders the main scene
 */
class Main extends BasisComponent {

  constructor(props) {
    super(props);

    this.state = {
      x: this.getOrientation() == 'portrait' ? 0 : -100,
      y: this.getOrientation() == 'portrait' ? 0 : 300,
      svgWidth: this.getWidth(),
      svgHeight: this.getHeight() / (this.getOrientation() == 'portrait' ? 2 : 1),
      widthViewBox: this.getOrientation() == 'portrait' ? '1400' : '1650',
      heightViewBox: this.getOrientation() == 'portrait' ? '1000' : '430',
      orientation: this.getOrientation(),
    };
  }

  componentWillMount() {
    this.props.executeGetCuts();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.wasErrorGettingCuts != nextProps.wasErrorGettingCuts && nextProps.wasErrorGettingCuts)
      this.showErrorMessage('Hubo un problema al traer los datos, por favor intentalo nuevamente');
  }

  render() {

    return (
      <StyleProvider style={getTheme(material)}>
        <Container style={style.container}>
          {
            this.props.isGettingCuts
            ? <Loading/>
            : this.getOrientation() == 'portrait'
            ? this.renderPortraitView()
            : this.renderLandscapeView()
          }
        </Container>
      </StyleProvider>
    );
  }
  /**
   * It renders a view in portrait orientation
   */
  renderPortraitView() {

    return (
      <CollapsingToolbar
        toolbarMaxHeight={this.getHeight() / 2.5}
        toolbarColor={primaryColor}
        title={'CORTES DE RES SECOBOL BEEF'}
        src={require('../../image/toolbarBackground.png')}
        rightItem={<Icon name={'md-cloud-download'} size={30} color={accentColor}/>}
        rightItemPress={() => this.props.executeGetCuts(true)}>
        <Content>

          <Cow
            x={this.state.x}
            y={this.state.y}
            svgWidth={this.state.svgWidth}
            svgHeight={this.state.svgHeight}
            heightViewBox={this.state.heightViewBox}
            widthViewBox={this.state.widthViewBox}
            onOpenDetail={!this.props.cuts || this.props.cuts.length != 0
              ? this._onOpenDetail.bind(this)
              : this.invokedShowErrorMessage.bind(this)}/>

          <Footer
            orientation={this.state.orientation}
            cuts={this.props.cuts}
            onOpenDetail={!this.props.cuts || this.props.cuts.length != 0
              ? this._onOpenDetail.bind(this)
              : this.invokedShowErrorMessage.bind(this)}/>
        </Content>
      </CollapsingToolbar>
    );
  }
  /**
   * It renders a view in landscape orientation
   */
  renderLandscapeView() {

    return (
      <Content>
        <Cow
          x={this.state.x}
          y={this.state.y}
          svgWidth={this.state.svgWidth}
          svgHeight={this.state.svgHeight}
          heightViewBox={this.state.heightViewBox}
          widthViewBox={this.state.widthViewBox}
          onOpenDetail={!this.props.cuts || this.props.cuts.length != 0
            ? this._onOpenDetail.bind(this)
            : this.invokedShowErrorMessage.bind(this)}/>

        <Footer
          orientation={this.state.orientation}
          cuts={this.props.cuts}
          onOpenDetail={!this.props.cuts || this.props.cuts.length != 0
            ? this._onOpenDetail.bind(this)
            : this.invokedShowErrorMessage.bind(this)}/>
      </Content>
    );
  }
  /**
   * Listener inherited of its father
   */
  onChageOrientation() {
    this.setState({
      x: this.getOrientation() == 'portrait' ? 0 : -100,
      y: this.getOrientation() == 'portrait' ? 0 : 300,
      svgWidth: this.getWidth(),
      svgHeight: this.getHeight() / (this.getOrientation() == 'portrait' ? 2 : 1),
      widthViewBox: this.getOrientation() == 'portrait' ? '1400' : '1650',
      heightViewBox: this.getOrientation() == 'portrait' ? '1000' : '430',
      orientation: this.getOrientation(),
    });
  }
  /**
   * Listener triggered when a user presses a part of the cow
   */
  _onOpenDetail(number) {
    this.props.navigator.push({
      screen: 'secobol.Detail',
      passProps: {
        number: number,
      },
      navigatorStyle: {
        navBarHidden: true,
      },
    });
  }
  /**
   * It invokes the show error message method from its father
   */
  invokedShowErrorMessage() {
    this.showErrorMessage('No hay datos, ¡tienes que obtener datos desde la nube!')
  }
}

const mapStateToProps = state => ({
  cuts: state.dataReducer.cutReducer.cuts,
  wasErrorGettingCuts: state.dataReducer.cutReducer.statusOfGettingCuts.wasError,
  isGettingCuts: state.dataReducer.cutReducer.statusOfGettingCuts.isGettingCuts,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  executeGetCuts: executeGetCuts,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Main);
