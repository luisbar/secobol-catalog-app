/***********************
 * Node modules import *
 ***********************/
import React, { Component } from 'react';

import {
  Dimensions,
  Platform
} from 'react-native';

import Toast from 'react-native-root-toast';
/******************
 * Project import *
 ******************/
import {
  textColor,
  dangerColor,
} from '../color.js';

class BasisComponent extends Component {

  constructor(props) {
    super(props);
    
    this.state = {
      toastWidth: this.getWidth()
    }

    //To register a listener for knowing when orientation has changed
    Dimensions.addEventListener('change', () => this.onChageOrientation());
  }
  /**
   * It returs the orientation of screen
   */
  getOrientation() {
    const dim = Dimensions.get('screen');
    return dim.height >= dim.width ? 'portrait' : 'landscape';
  }
  /**
   * It return the widht of screen
   */
  getWidth() {
    return Dimensions.get('screen').width;
  }
  /**
   * It return the height of screen
   */
  getHeight() {
    return Dimensions.get('screen').height;
  }
  /**
   * Listener for knowing when the orientation of a smarthpone
   * has changed
   */
  onChageOrientation() {
    this.setState({ toastWidth: this.getWidth() })
  }
  /**
   * It shows a message according platform
   */
  showErrorMessage(message) {
    if (Platform.OS == 'ios')
      this.showIosMessage(message, dangerColor);
    else
      this.showAndroidMessage(message, dangerColor);
  }
  /**
   * It shows an error message for ios
   */
  showIosMessage(message, color) {
    Toast.show(message, {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      textColor: textColor,
      backgroundColor: color,
      customStyle: {
        width: this.state.toastWidth,
      }
    });
  }
  /**
   * It shows an error message for android
   */
  showAndroidMessage(message, color) {
    this.props.navigator.showSnackbar({
      text: message,
      textColor: textColor,
      backgroundColor: color,
      duration: 'long',
    });
  }
}

export default BasisComponent;
