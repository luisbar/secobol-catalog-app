import { Navigation } from 'react-native-navigation';

import { registerScenes } from './src/register.js';

registerScenes();

Navigation.startSingleScreenApp({
  screen: {
    screen: 'secobol.Main',
    navigatorStyle: {
      navBarHidden: true,// Esconde el toolbar
    },
  },
  appStyle: {
    orientation: 'auto',// Orientación de toda la app
  },
  animationType: 'slide-down',// Tipo de animación de la transición('none', 'slide-down', 'fade')
});

// console.disableYellowBox = true;
